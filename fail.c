
#include "fail.h"

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

static int warning_disabled = 0;

void fail(const char * s, ...)
{
   va_list args;
   va_start( args, s );
   vfprintf( stderr, s, args );
   va_end( args );
   exit(1);
}


void warn( const char *s, ... )
{
   va_list args;
   if( warning_disabled )
   {
      return;
   }
   va_start( args, s );
   vfprintf( stderr, s, args );
   va_end( args );
}


void disablewarn()
{
   warning_disabled = 1;
}
