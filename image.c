
#include "image.h"
#include <stdlib.h>


image_t *imagecreate( int width, int height )
{
   int i;
   image_t *image;

   image = (image_t*)malloc( sizeof(image_t) );
   image->width  = width;
   image->height = height;
   image->xbytes = (width + 7) / 8;
   image->bitmap = (png_bytep)calloc( image->xbytes, height );
   image->rowps  = (png_bytep*)malloc( sizeof( png_bytep ) * height );

   for( i = 0; i < height; i ++ )
   {
      image->rowps[i] = image->bitmap + (i * image->xbytes);
   }

   return image;
}


void imagedestroy( image_t *image )
{
   free( image->bitmap );
   free( image->rowps );
   free( image );
   image = NULL;
}



static inline int imagegetpixel( image_t *image, int x, int y )
{
   return (image->bitmap[image->xbytes * y + (x/8)] & (0x80 >> (x & 7))) ? 1 : 0;
}


static inline void imagesetpixel( image_t *image, int x, int y, int c )
{
   if( c )
   {
      image->bitmap[image->xbytes * y + (x/8)] |= (0x80 >> (x & 7));
   }
   else
   {
      image->bitmap[image->xbytes * y + (x/8)] &= ~(0x80 >> (x & 7));
   }
}


/* flip image 180 degrees */
void imageflip( image_t *image )
{
   int x, x2, y, y2, c, c2;

   for( y = 0; y < (image->height) >> 1; ++y )
   {
      y2 = image->height - 1 - y;
      for( x = 0; x < image->width; ++x )
      {
         /* exchage pixel from top left with bottom right and so on */
         x2 = image->width - 1 - x;
         c  = imagegetpixel( image, x, y );
         c2 = imagegetpixel( image, x2, y2 );
         imagesetpixel( image, x, y, c2 );
         imagesetpixel( image, x2, y2, c );
      }
   }

   if( image->height & 1 )
   {
      /* odd number of lines, so mirror center line */
      y = image->height/2;
      for( x = 0; x < (image->width) >> 1; ++x )
      {
         x2 = image->width - 1 - x;
         c  = imagegetpixel( image, x, y );
         c2 = imagegetpixel( image, x2, y );
         imagesetpixel( image, x, y, c2 );
         imagesetpixel( image, x2, y, c );
      }
   }
}
