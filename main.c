
#include "image.h"
#include "moon.h"
#include "pngwrite.h"
#include "stars.h"
#include "fail.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define VERSION "1.3"

static inline void usage( char *name )
{
   char *base = name;
   char *c;

   for( c = name+1; *c; c++)
   {
      if( *(c-1) == '/' ) base = c;
   }

   printf( "this is pngphoon version " VERSION "\n\n"
           "usage: %s options (* = mandatory)\n"
           "\t-w # width*\n"
           "\t-h # height*\n"
           "\t-f $ filename*\n"
           "\t-x # number of moons in x axis (for multihead configurations)\n"
           "\t-y # number of moons in y axis (for multihead configurations)\n"
           "\t-s # star density\n"
           "\t-F $ foreground color\n"
           "\t-B $ background color\n"
           "\t-b   black (no earthlight)\n"
           "\t-S   southern hemisphere (rotate image 180 degrees)\n"
           "\t-q   quiet (disable warnings)\n", base );
   exit(0);
}


static png_color hex2col( const char *hexstr, const char *colname )
{
   int convfail = 0;
   png_color retval;
   char *endstr;
   unsigned long colval = strtoul( hexstr, &endstr, 16 );

   if( *endstr )
   {
      convfail = 1;
   }
   else
   {
      switch( endstr-hexstr )
      {
      case 3:
         retval.red   = ((colval & 0xf00) >> 8) * 0x11;
         retval.green = ((colval & 0x0f0) >> 4) * 0x11;
         retval.blue  = ((colval & 0x00f)     ) * 0x11;
         break;
      case 6:
         retval.red   = (colval & 0xff0000) >> 16;
         retval.green = (colval & 0x00ff00) >> 8;
         retval.blue  = (colval & 0x0000ff);
         break;
      default:
         convfail = 1;
      }
   }
   if( convfail )
   {
      fail( "%s color '%s' does not match 3 or 6 digit hex code\n",
            colname, hexstr );
   }

   return retval;
}


int main( int argc, char *argv[] )
{
   unsigned int width      = 0;
   unsigned int height     = 0;
   unsigned int drawheight = 0;
   unsigned int xmoons     = 1;
   unsigned int ymoons     = 1;
   int          stars      = 50;
   char         *bgcolor   = NULL;
   char         *fgcolor   = NULL;
   char         *filename  = NULL;
   int          black      = 0;
   int          southern   = 0;
   image_t      *image     = NULL;
   moon_t       *moon      = NULL;
   int          c;
   unsigned int x;
   unsigned int y;
   png_color    palette[2] = { { 0x00, 0x00, 0x00 }, { 0xff, 0xff, 0xff } };

   opterr = 0;

   if( argc == 1 )
   {
      usage( argv[0] );
   }
   while( (c = getopt( argc, argv, "w:h:x:y:s:F:B:f:bSq" )) != EOF )
   {
      switch (c)
      {
      case 'w':
         width    = strtoul( optarg, NULL, 0 );
         break;
      case 'h':
         height   = strtoul( optarg, NULL, 0 );
         break;
      case 'f':
         filename = optarg;
         break;
      case 'x':
         xmoons   = strtoul( optarg, NULL, 0 );
         break;
      case 'y':
         ymoons   = strtoul( optarg, NULL, 0 );
         break;
      case 's':
         stars    = strtoul( optarg, NULL, 0 );
         break;
      case 'F':
         fgcolor  = optarg;
         break;
      case 'B':
         bgcolor  = optarg;
         break;
      case 'b':
         black    = 1;
         break;
      case 'S':
         southern = 1;
         break;
      case 'q':
         disablewarn();
         break;
      default:
         fail("unknown option: '%c'\n", c);
      }
   }

   if( !width || !height || !filename )
   {
      fail( "you need to specify at least width, height and filename\n" );
   }

   moon = mooncreate();

   if( width < moon->width )
   {
      warn( "image not wide enough for anything, just creating stars\n" );
      xmoons = 0;
   }

   if( width < moon->width * xmoons )
   {
      warn( "decreasing number of moons in x axis from %d ", xmoons );
      while( width < moon->width * --xmoons );
      warn( "to %d moons.\n", xmoons );
   }

   drawheight = height;
   if( height < moon->height )
   {
      warn( "image not high enough for moon, using truncate mode\n" );
      drawheight = moon->height;
      ymoons = 1;
   }
   else
   {
      if( height < moon->height * ymoons )
      {
         warn( "decreasing number of moons in y axis from %d ", ymoons );
         /* this is not nice, but works: decreasing number of moons until they fit */
         while( width < (moon->width * --ymoons) );
         warn( "to %d moons.\n", ymoons );
      }
   }

   image = imagecreate( width, drawheight );
   if( (xmoons > 0) && (ymoons > 0) )
   {
      scarymonster( image, stars );
      for( y = (drawheight/ymoons)/2; y < height; y += (drawheight/ymoons) )
      {
         for( x = (width/xmoons)/2; x < width; x += (width/xmoons) )
         {
            mooncopy( image, moon, x, y, black );
         }
      }
   }
   else
   {
      scarymonster( image, stars * 2 );
   }

   if( southern )
   {
      imageflip( image );
   }

   if( bgcolor )
   {
      palette[0] = hex2col( bgcolor, "background" );
   }
   if( fgcolor )
   {
      palette[1] = hex2col( fgcolor, "foreground" );
   }

   pngwrite( image, filename, height, (png_colorp) &palette[0] );
   imagedestroy( image );
   moondestroy( moon );

   return 0;
}
