
#ifndef _PHASE_H_
#define _PHASE_H_ _PHASE_H_

#include "tws.h"

double phase( double pdate, double *pphase, double *mage, double *dist,
              double *angdia, double *sudist, double *suangdia );

double jtime( struct tws *t );

#endif
